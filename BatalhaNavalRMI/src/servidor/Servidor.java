package servidor;

import cliente.ClienteInterface;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelos.*;

public class Servidor implements ServidorInterface {

    final int JOGADOR_EM_INICIO = 1;
    final int JOGADOR_EM_SALA = 2;
    final int JOGADOR_AGUARDANDO_INICIO_DE_JOGO = 3;
    final int MAPA_CONFIGURADO = 4;

    private ArrayList<ClienteInterface> clientes = new ArrayList<>();
    private ArrayList<Room> salas = new ArrayList<>();

    public static void main(String[] args) {
        try {
            Servidor servidor = new Servidor();
            ServidorInterface stub = (ServidorInterface) UnicastRemoteObject.exportObject(servidor, 0);

            Registry registry = LocateRegistry.createRegistry(6789);
            registry.bind("Jogo", stub);

            System.out.println("Servidor iniciado...");
        } catch (RemoteException | AlreadyBoundException e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void registrar(ClienteInterface cliente) throws RemoteException {
        this.clientes.add(cliente);
        cliente.notificar("Cliente registrado com sucesso!");
        cliente.notificar("Comandos: /listar, /criar, /rooms, /entrar, /iniciar (apenas se estiver na room)");
        cliente.waitCommand();
    }

    @Override
    public void registrarSala(UUID clienteID, int roomID, String roomName) throws RemoteException {
        ClienteInterface cliente = this.searchCliente(clienteID);
        Room room = new Room(roomID, roomName, clienteID);
        this.salas.add(room);

        cliente.setMyRoomId(roomID);
        cliente.setStatus(JOGADOR_EM_SALA);
        cliente.notificar("Sala registrada com sucesso!");
    }

    @Override
    public void runCommand(UUID clienteID, String comando) throws RemoteException {
        int id = 0;
        if (comando.contains(":")) {
            id = Integer.parseInt(comando.split(":")[1]);
            comando = comando.split(":")[0];

            if (id == 0) {
                this.searchCliente(clienteID).notificar("RoomID invalido!");
            }
        }

        switch (comando) {
            case "/listar":
                this.listarJogadores(clienteID);
                break;
            case "/criar":
                this.criarSala(clienteID);
                break;
            case "/rooms":
                this.listarSalas(clienteID);
                break;
            case "/entrar":
                this.entrarNaRoom(clienteID, id);
                break;
            case "/iniciar":
                this.iniciarJogo(clienteID);
                break;
            default:
                break;
        }
    }

    private void listarJogadores(UUID clienteID) throws RemoteException {
        String resp = "+----- JOGADORES -----+\n";
        for (int i = 0; i < clientes.size(); i++) {
            resp += "-> " + clientes.get(i).getNome() + "\n";
        }
        resp += "+---------------------+\n";
        this.searchCliente(clienteID).notificar(resp);
    }

    private void criarSala(UUID clienteID) throws RemoteException {
        this.searchCliente(clienteID).criarSala();
    }

    private void listarSalas(UUID clienteID) throws RemoteException {
        String resp = "+----- SALAS -----+\n";
        for (int i = 0; i < salas.size(); i++) {
            resp += "-> ID: " + salas.get(i).getId() + " - Nome: " + salas.get(i).getNome() + "\n";
        }
        resp += "+---------------------+\n";
        this.searchCliente(clienteID).notificar(resp);
    }

    private void entrarNaRoom(UUID clienteID, int roomID) throws RemoteException {
        System.out.println(clienteID + " -- " + roomID);
        Room room = this.searchRoom(roomID);
        ClienteInterface cliente = this.searchCliente(clienteID);

        if (room.getJogadores().size() <= 2) {
            room.addJogador(clienteID);
            cliente.notificar("Voce acaba de entrar na sala " + room.getNome() + "(" + room.getId() + ")");
            cliente.setMyRoomId(roomID);
            cliente.setStatus(JOGADOR_EM_SALA);
        } else {
            cliente.notificar("A sala esta LOTADA! Tente outra...");
        }
    }

    private void iniciarJogo(UUID clienteID) throws RemoteException {
        Room room = this.searchRoom(this.searchCliente(clienteID).getMyRoomId());

        System.out.println(this.searchCliente(room.getJogadores().get(0)).getStatus());
        System.out.println(this.searchCliente(room.getJogadores().get(1)).getStatus());

        if (this.searchCliente(clienteID).getStatus() == JOGADOR_EM_SALA) {
            this.searchCliente(clienteID).setStatus(JOGADOR_AGUARDANDO_INICIO_DE_JOGO);

            while (this.searchCliente(room.getJogadores().get(0)).getStatus() != JOGADOR_AGUARDANDO_INICIO_DE_JOGO
                    || this.searchCliente(room.getJogadores().get(1)).getStatus() != JOGADOR_AGUARDANDO_INICIO_DE_JOGO) {
                if (this.searchCliente(room.getJogadores().get(0)).getStatus() != JOGADOR_AGUARDANDO_INICIO_DE_JOGO) {
                    this.searchCliente(room.getJogadores().get(0)).notificar("Aguardando voce iniciar o JOGO...");
                    this.searchCliente(room.getJogadores().get(1)).notificar("Aguardando jogador adversario iniciar o JOGO...");
                }
                if (this.searchCliente(room.getJogadores().get(1)).getStatus() != JOGADOR_AGUARDANDO_INICIO_DE_JOGO) {
                    this.searchCliente(room.getJogadores().get(1)).notificar("Aguardando voce iniciar o JOGO...");
                    this.searchCliente(room.getJogadores().get(0)).notificar("Aguardando jogador adversario iniciar o JOGO...");
                }

                try {
                    Thread.sleep(5 * 1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            System.out.println("JOGADOR_AGUARDANDO_INICIO_DE_JOGO OK");
            System.out.println(this.searchCliente(room.getJogadores().get(0)).getStatus());
            System.out.println(this.searchCliente(room.getJogadores().get(1)).getStatus());

            this.searchCliente(room.getJogadores().get(0)).configurarMapa();
            this.searchCliente(room.getJogadores().get(1)).configurarMapa();
            
            System.out.println("CHAMADA DE CONFIGURAR MAPA OK");
            System.out.println(this.searchCliente(room.getJogadores().get(0)).getStatus());
            System.out.println(this.searchCliente(room.getJogadores().get(1)).getStatus());

            while (this.searchCliente(room.getJogadores().get(0)).getStatus() != MAPA_CONFIGURADO
                    || this.searchCliente(room.getJogadores().get(1)).getStatus() != MAPA_CONFIGURADO) {
                if (this.searchCliente(room.getJogadores().get(0)).getStatus() != MAPA_CONFIGURADO) {
                    this.searchCliente(room.getJogadores().get(0)).notificar("Aguardando voce configurar o mapa...");
                    this.searchCliente(room.getJogadores().get(1)).notificar("Aguardando jogador adversario configurar o mapa...");
                }
                if (this.searchCliente(room.getJogadores().get(1)).getStatus() != MAPA_CONFIGURADO) {
                    this.searchCliente(room.getJogadores().get(1)).notificar("Aguardando voce configurar o mapa...");
                    this.searchCliente(room.getJogadores().get(0)).notificar("Aguardando jogador adversario configurar o mapa...");
                }

                try {
                    Thread.sleep(5 * 1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            System.out.println("JOGO INICIADO!!");

        } else {
            this.searchCliente(clienteID).notificar("Impossivel iniciar, voce nao se encontra em uma sala.");
        }
    }

    private ClienteInterface searchCliente(UUID clienteID) throws RemoteException {
        for (int i = 0; i < clientes.size(); i++) {
            if (clienteID.equals(clientes.get(i).getId())) {
                return clientes.get(i);
            }
        }

        return null;
    }

    private Room searchRoom(int roomID) throws RemoteException {
        for (int i = 0; i < salas.size(); i++) {
            if (roomID == salas.get(i).getId()) {
                return salas.get(i);
            }
        }

        return null;
    }

}