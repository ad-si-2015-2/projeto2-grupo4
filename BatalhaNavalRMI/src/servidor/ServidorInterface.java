package servidor;

import cliente.ClienteInterface;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public interface ServidorInterface extends Remote {

    /**
     * Registrar um cliente na memoria do servidor
     *
     * @param cliente
     * @throws RemoteException
     */
    public void registrar(ClienteInterface cliente) throws RemoteException;

    /**
     *
     * @param clienteID
     * @param roomID
     * @param roomName
     * @throws RemoteException
     */
    public void registrarSala(UUID clienteID, int roomID, String roomName) throws RemoteException;

    /**
     *
     * @param clienteID
     * @param comando
     * @throws RemoteException
     */
    public void runCommand(UUID clienteID, String comando) throws RemoteException;
}
