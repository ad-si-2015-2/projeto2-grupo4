package cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.UUID;
import servidor.ServidorInterface;

public class Cliente implements ClienteInterface {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static Registry registry;
    static ServidorInterface servidor;
    private String nome;
    private UUID id;

    private int myRoomId = 0;
    private int status = 1;
    private int myMap[][];

    static {
        try {
            registry = LocateRegistry.getRegistry("localhost", 6789);
            servidor = (ServidorInterface) registry.lookup("Jogo");

        } catch (RemoteException ex) {
            System.err.println("Nao foi possivel conectar ao servidor. Servidor esta ativo?");
            System.err.println(ex);
            System.exit(0);
        } catch (NotBoundException ex) {
            System.err.println("Nao foi possivel encontrar o registro. Variavel esta correta?");
            System.err.println(ex);
            System.exit(0);
        }
    }

    /**
     * Registrar um cliente na memoria do Servidor
     *
     * @param cliente
     * @throws RemoteException
     */
    public void registrar(ClienteInterface cliente) throws RemoteException {
        servidor.registrar(cliente);
    }

    public static void main(String args[]) throws Exception {
        System.out.println("Informe seu nome: ");

        Cliente cliente = new Cliente();
        cliente.nome = reader.readLine();
        cliente.id = UUID.randomUUID();

        ClienteInterface stub = (ClienteInterface) UnicastRemoteObject.exportObject(cliente, 0);

        Random gerador = new Random();
        Registry registry = LocateRegistry.createRegistry(gerador.nextInt(1000) + 6000);
        registry.bind("Cliente", stub);

        cliente.registrar(cliente);
    }

    @Override
    public void notificar(String mensagem) throws RemoteException {
        System.out.println(mensagem);
    }

    @Override
    public String getNome() throws RemoteException {
        return this.nome;
    }

    @Override
    public UUID getId() throws RemoteException {
        return this.id;
    }

    @Override
    public int getMyRoomId() throws RemoteException {
        return this.myRoomId;
    }

    @Override
    public void setMyRoomId(int roomID) throws RemoteException {
        this.myRoomId = roomID;
    }

    @Override
    public int getStatus() throws RemoteException {
        return this.status;
    }

    @Override
    public void setStatus(int novoStatus) throws RemoteException {
        this.status = novoStatus;
    }

    @Override
    public void waitCommand() throws RemoteException {
        String comando;
        try {
            while (true) {
                comando = reader.readLine();
                servidor.runCommand(id, comando);
                if (comando.equals("/iniciar")) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void criarSala() throws RemoteException {
        System.out.println("Informe o nome da sala: ");
        try {
            String roomNome = reader.readLine();
            //Criar funcao para numero aleatorio
            servidor.registrarSala(id, 1, roomNome);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public int[][] configurarMapa() {
        Random gerador = new Random();

        int mapa1[][] = {
            {1, 1, 1, 0, 0, 1, 0, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 1, 1},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 1, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 1, 1, 1, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
            {1, 1, 1, 1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
        };

        int mapa2[][] = {
            {0, 0, 0, 1, 1, 0, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 1, 1, 0, 0, 1},
            {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 1, 1, 1},
            {1, 1, 1, 1, 0, 0, 0, 0, 0, 0}
        };

        int mapa3[][] = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 1, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 1, 0, 1},
            {0, 0, 1, 1, 1, 0, 0, 0, 0, 1},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 1, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {1, 0, 1, 1, 1, 1, 0, 0, 0, 1}
        };

        int r = gerador.nextInt(4);
        int[][] mapa = mapa1.clone();

        if (r % 3 == 0) {
            mapa = mapa1.clone();
        }
        if (r % 3 == 1) {
            mapa = mapa2.clone();
        }
        if (r % 3 == 2) {
            mapa = mapa3.clone();
        }
        
        return mapa;
    }
}
