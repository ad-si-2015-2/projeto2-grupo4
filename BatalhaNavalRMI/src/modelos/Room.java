package modelos;

import java.util.ArrayList;
import java.util.UUID;

public class Room {

    private final int id;
    private String nome;
    private ArrayList<UUID> jogadores = new ArrayList<>();

    public Room(int roomID, String nome, UUID playerID) {
        System.out.println(playerID + " " + roomID + " " + nome);
        this.id = roomID;
        this.nome = nome;
        this.jogadores.add(playerID);
    }
    
    public void addJogador(UUID playerID){
        this.jogadores.add(playerID);
    }

    public int getId() {
        return id;
    }

    public ArrayList<UUID> getJogadores() {
        return jogadores;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
