package cliente;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public interface ClienteInterface extends Remote {

    /**
     * Envia mensagens para o cliente
     * @param mensagem
     * @throws RemoteException
     */
    public void notificar(String mensagem) throws RemoteException;
    
    /**
     *
     * @return
     * @throws RemoteException
     */
    public String getNome() throws RemoteException;
    
    /**
     *
     * @return
     * @throws RemoteException
     */
    public UUID getId() throws RemoteException;
    
    /**
     *
     * @return
     * @throws RemoteException
     */
    public int getMyRoomId() throws RemoteException;
    
    /**
     *
     * @param roomID
     * @throws RemoteException
     */
    public void setMyRoomId(int roomID)throws RemoteException;
    
    /**
     *
     * @return
     * @throws RemoteException
     */
    public int getStatus() throws RemoteException;

    /**
     *
     * @param novoStatus
     * @throws RemoteException
     */
    public void setStatus(int novoStatus) throws RemoteException;
    
    /**
     *
     * @throws RemoteException
     */
    public void waitCommand() throws RemoteException;
    
    /**
     *
     * @throws RemoteException
     */
    public void criarSala() throws RemoteException;
    
    /**
     *
     * @return 
     * @throws java.rmi.RemoteException 
     */
    public int[][] configurarMapa() throws RemoteException;
}
